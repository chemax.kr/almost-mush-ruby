# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160724165038) do

  create_table "characters", force: :cascade do |t|
    t.integer  "flight_id"
    t.integer  "user_id"
    t.integer  "character_description_id"
    t.integer  "hp"
    t.integer  "morale"
    t.integer  "pa"
    t.integer  "pm"
    t.integer  "room_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "flights", force: :cascade do |t|
    t.string   "status"
    t.string   "size"
    t.string   "mission"
    t.integer  "day"
    t.integer  "cycle"
    t.integer  "armor"
    t.integer  "shield"
    t.boolean  "shield_enabled"
    t.integer  "fuel"
    t.integer  "oxygen"
    t.datetime "date_end"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "item_descriptions", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "charges_start"
    t.integer  "charges_max"
    t.string   "type"
    t.boolean  "can_stack"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "items", force: :cascade do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "item_description_id"
    t.string   "item_status"
    t.integer  "item_charges"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
