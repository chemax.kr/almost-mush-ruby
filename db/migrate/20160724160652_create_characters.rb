class CreateCharacters < ActiveRecord::Migration[5.0]
  def change
    create_table :characters do |t|
      t.integer :flight_id
      t.integer :user_id
      t.integer :character_description_id
      t.integer :hp
      t.integer :morale
      t.integer :pa
      t.integer :pm
      t.integer :room_id

      t.timestamps
    end
  end
end
