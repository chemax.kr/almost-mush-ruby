class CreateItemDescriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :item_descriptions do |t|
      t.string :name
      t.string :description
      t.integer :charges_start
      t.integer :charges_max
      t.string :type
      t.boolean :can_stack

      t.timestamps
    end
  end
end
