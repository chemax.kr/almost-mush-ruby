class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.integer :owner_id
      t.string :owner_type
      t.integer :item_description_id
      t.string :item_status
      t.integer :item_charges

      t.timestamps
    end
  end
end
