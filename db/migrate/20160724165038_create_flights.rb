class CreateFlights < ActiveRecord::Migration[5.0]
  def change
    create_table :flights do |t|
      t.string :status
      t.string :size
      t.string :mission
      t.integer :day
      t.integer :cycle
      t.integer :armor
      t.integer :shield
      t.boolean :shield_enabled
      t.integer :fuel
      t.integer :oxygen
      t.datetime :date_end

      t.timestamps
    end
  end
end
