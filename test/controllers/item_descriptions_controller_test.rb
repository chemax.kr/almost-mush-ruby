require 'test_helper'

class ItemDescriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @item_description = item_descriptions(:one)
  end

  test "should get index" do
    get item_descriptions_url
    assert_response :success
  end

  test "should get new" do
    get new_item_description_url
    assert_response :success
  end

  test "should create item_description" do
    assert_difference('ItemDescription.count') do
      post item_descriptions_url, params: { item_description: { can_stack: @item_description.can_stack, charges_max: @item_description.charges_max, charges_start: @item_description.charges_start, description: @item_description.description, name: @item_description.name, type: @item_description.type } }
    end

    assert_redirected_to item_description_url(ItemDescription.last)
  end

  test "should show item_description" do
    get item_description_url(@item_description)
    assert_response :success
  end

  test "should get edit" do
    get edit_item_description_url(@item_description)
    assert_response :success
  end

  test "should update item_description" do
    patch item_description_url(@item_description), params: { item_description: { can_stack: @item_description.can_stack, charges_max: @item_description.charges_max, charges_start: @item_description.charges_start, description: @item_description.description, name: @item_description.name, type: @item_description.type } }
    assert_redirected_to item_description_url(@item_description)
  end

  test "should destroy item_description" do
    assert_difference('ItemDescription.count', -1) do
      delete item_description_url(@item_description)
    end

    assert_redirected_to item_descriptions_url
  end
end
