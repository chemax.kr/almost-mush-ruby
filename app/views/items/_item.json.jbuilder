json.extract! item, :id, :owner_id, :owner_type, :item_id, :item_status, :item_charges, :created_at, :updated_at
json.url item_url(item, format: :json)