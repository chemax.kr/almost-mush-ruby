json.extract! item_description, :id, :name, :description, :charges_start, :charges_max, :type, :can_stack, :created_at, :updated_at
json.url item_description_url(item_description, format: :json)