json.extract! flight, :id, :status, :size, :mission, :day, :cycle, :armor, :shield, :shield_enabled, :fuel, :oxygen, :date_end, :created_at, :updated_at
json.url flight_url(flight, format: :json)