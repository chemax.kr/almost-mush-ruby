json.extract! character, :id, :flight_id, :user_id, :character_description_id, :hp, :morale, :pa, :pm, :room_id, :created_at, :updated_at
json.url character_url(character, format: :json)