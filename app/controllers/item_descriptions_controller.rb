class ItemDescriptionsController < ApplicationController
  before_action :set_item_description, only: [:show, :edit, :update, :destroy]

  # GET /item_descriptions
  # GET /item_descriptions.json
  def index
    @item_descriptions = ItemDescription.all
  end

  # GET /item_descriptions/1
  # GET /item_descriptions/1.json
  def show
  end

  # GET /item_descriptions/new
  def new
    @item_description = ItemDescription.new
  end

  # GET /item_descriptions/1/edit
  def edit
  end

  # POST /item_descriptions
  # POST /item_descriptions.json
  def create
    @item_description = ItemDescription.new(item_description_params)

    respond_to do |format|
      if @item_description.save
        format.html { redirect_to @item_description, notice: 'Item description was successfully created.' }
        format.json { render :show, status: :created, location: @item_description }
      else
        format.html { render :new }
        format.json { render json: @item_description.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /item_descriptions/1
  # PATCH/PUT /item_descriptions/1.json
  def update
    respond_to do |format|
      if @item_description.update(item_description_params)
        format.html { redirect_to @item_description, notice: 'Item description was successfully updated.' }
        format.json { render :show, status: :ok, location: @item_description }
      else
        format.html { render :edit }
        format.json { render json: @item_description.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /item_descriptions/1
  # DELETE /item_descriptions/1.json
  def destroy
    @item_description.destroy
    respond_to do |format|
      format.html { redirect_to item_descriptions_url, notice: 'Item description was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item_description
      @item_description = ItemDescription.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_description_params
      params.require(:item_description).permit(:name, :description, :charges_start, :charges_max, :type, :can_stack)
    end
end
