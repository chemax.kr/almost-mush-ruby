class Item < ApplicationRecord
  belongs_to :item_description
  belongs_to :owner, polymorphic: true

  def self.create_for(owner, item_description)
    item = Item.new(owner: owner, item_description: item_description)
    item.item_status  = 'ok'
    item.item_charges = item_description.charges_start
    item.save
  end
end
