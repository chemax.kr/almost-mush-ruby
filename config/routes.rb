Rails.application.routes.draw do
  resources :flights
  resources :item_descriptions
  resources :characters
  resources :items
  resources :games
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  match '/help', to: 'static_pages#help', via: 'get'
end
